#!/bin/bash
ZABBIX_WEB_CONF="/etc/zabbix/web/zabbix.conf.php"

# Cheack variables 
if [[ ! "$MYSQL_USER" ]];then
    echo "MYSQL_USER is not defined use: 'zabbix'"
    MYSQL_USER=zabbix
fi
if [[ ! "$MYSQL_PASSWORD" ]];then
    echo "MYSQL_PASSWORD is not defined use: 'password'"
    MYSQL_PASSWORD=password
fi
if [[ ! "$MYSQL_DATABASE" ]];then
    echo "MYSQL_DATABASE is not defined use: 'zabbix'"
    MYSQL_DATABASE=zabbix
fi
if [[ ! "$DB_SERVER_HOST" ]];then
    echo "DB_SERVER_HOST is not defined use: 'localhost'"
    DB_SERVER_HOST=localhost
fi
if [[ ! "$ZBX_SERVER_HOST" ]];then
    echo "ZBX_SERVER_HOST is not defined use: 'zabbix-server'"
    ZBX_SERVER_HOST=zabbix-server
fi

echo "php-FPM setup ..."
sed -i -e "s/;\ php_value\[date\.timezone.*$/php_value\[date\.timezone\]\ =\ UTC/" /etc/zabbix/php-fpm.conf

echo "Nginx setup ..."
rm -rf /etc/nginx/sites-enabled/default
sed -i -e "s/^#[[:space:]]*listen.*;$/\tlisten\ 80\;/" /etc/zabbix/nginx.conf

# Setup Zabbix WEB config
cp /usr/share/zabbix/conf/zabbix.conf.php.example $ZABBIX_WEB_CONF
sed -i -e "s/\$DB\['SERVER'\].*$/\$DB['SERVER']\t= '$DB_SERVER_HOST';/" $ZABBIX_WEB_CONF
sed -i -e "s/\$DB\['DATABASE'\].*$/\$DB['DATABASE']\t = '$MYSQL_DATABASE';/" $ZABBIX_WEB_CONF
sed -i -e "s/\$DB\['USER'\].*$/\$DB['USER']\t = '$MYSQL_USER';/" $ZABBIX_WEB_CONF
sed -i -e "s/\$DB\['PORT'\].*$/\$DB['PORT']\t = '3306';/" $ZABBIX_WEB_CONF
sed -i -e "s/\$DB\['PASSWORD'\].*$/\$DB['PASSWORD']\t = '$MYSQL_PASSWORD';/" $ZABBIX_WEB_CONF
sed -i -e "s/\$ZBX_SERVER\t.*;$/\$ZBX_SERVER\t = '$ZBX_SERVER_HOST';/g" $ZABBIX_WEB_CONF

echo "Start Supervisor"
exec "$@"