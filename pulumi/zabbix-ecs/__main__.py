import pulumi
from pulumi import export, ResourceOptions, Output
import pulumi_aws as aws
import json

# Read configs
config = pulumi.Config()

db_name = config.require('db_name')
db_username = config.require('db_username')
db_password = config.require_secret('db_password')
nginx_tag = config.require('nginx_tag')
zabbix_tag = config.require('zabbix_tag')

# Create an ECS cluster to run a container-based service.
cluster = aws.ecs.Cluster('zabbix')

# Read back the default VPC and public subnet
default_vpc = aws.ec2.get_vpc(default='true')
default_vpc_subnets = aws.ec2.get_subnet_ids(vpc_id=default_vpc.id)

# Create a Security Groups
nginx_sg = aws.ec2.SecurityGroup(
    'nginx-sg',
    vpc_id=default_vpc.id,
    description='Allow HTTP traffic',
    ingress=[{
        'protocol': 'tcp',
        'from_port': 80,
        'to_port': 80,
        'cidr_blocks': ['0.0.0.0/0'],
    }],
    egress=[{
        'protocol': '-1',
        'from_port': 0,
        'to_port': 0,
        'cidr_blocks': ['0.0.0.0/0'],
    }]
)

zabbix_sg = aws.ec2.SecurityGroup(
    'zabbix-sg',
    vpc_id=default_vpc.id,
    description='Zabbix SG',
    ingress=[{
        'protocol': 'tcp',
        'from_port': 10051,
        'to_port': 10051,
        'cidr_blocks': ['0.0.0.0/0']
    }, {
        'protocol': 'tcp',
        'from_port': 10050,
        'to_port': 10050,
        'cidr_blocks': ['0.0.0.0/0']
    }],
    egress=[{
        'protocol': '-1',
        'from_port': 0,
        'to_port': 0,
        'cidr_blocks': ['0.0.0.0/0'],
    }]
)

rds_sg = aws.ec2.SecurityGroup(
    'rds-sg',
    vpc_id=default_vpc.id,
    description='Allow Mysql access',
    ingress=[{
        'protocol': 'tcp',
        'from_port': 3306,
        'to_port': 3306,
        'security_groups': [nginx_sg.id, zabbix_sg.id],
    }],
    egress=[{
        'protocol': '-1',
        'from_port': 0,
        'to_port': 0,
        'cidr_blocks': ['0.0.0.0/0'],
    }]
)

# Create a load balancer to listen for HTTP traffic on port 80.
alb = aws.lb.LoadBalancer('nginx-alb',
    security_groups=[nginx_sg.id],
    subnets=default_vpc_subnets.ids
)

atg = aws.lb.TargetGroup('nginx-tg',
    port=80,
    protocol='HTTP',
    target_type='ip',
    vpc_id=default_vpc.id
)

wl = aws.lb.Listener('nginx',
    load_balancer_arn=alb.arn,
    port=80,
    default_actions=[{
        'type': 'forward',
        'target_group_arn': atg.arn
    }]
)

# NLB for zabbix server backend
zabbix_nlb = aws.lb.LoadBalancer("zabbix-nlb",
    internal=False,
    load_balancer_type="network",
    subnets=default_vpc_subnets.ids,
)

zabbix_tg = aws.lb.TargetGroup('zabbix-tg',
    port=10051,
    protocol='TCP',
    target_type='ip',
    vpc_id=default_vpc.id
)

nlb_listener = aws.lb.Listener('zabbix-tcp',
    load_balancer_arn=zabbix_nlb.arn,
    port=10051,
    protocol="TCP",
    default_actions=[{
        'type': 'forward',
        'target_group_arn': zabbix_tg.arn
    }]
)

# Create an IAM role that can be used by our service's task.
role = aws.iam.Role('task-exec-role',
    assume_role_policy=json.dumps({
        'Version': '2008-10-17',
        'Statement': [{
            'Sid': '',
            'Effect': 'Allow',
            'Principal': {
                'Service': 'ecs-tasks.amazonaws.com'
            },
            'Action': 'sts:AssumeRole',
        }]
    })
)

policy = aws.iam.RolePolicy("fargate-policy",
    role=role,
    policy=Output.from_input({
        "Version": "2012-10-17",
        "Statement": [{
            "Action": ["logs:*", "cloudwatch:*"],
            "Resource": "*",
            "Effect": "Allow",
        }],
    }),
    )

rpa = aws.iam.RolePolicyAttachment('task-exec-policy',
    role=role.name,
    policy_arn='arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'
)

# RDS
default = aws.rds.SubnetGroup("default",
    subnet_ids=default_vpc_subnets.ids
)

mysql = aws.rds.Instance("zabbix-mysql",
    allocated_storage=10,
    engine="mysql",
    engine_version="5.7",
    instance_class="db.t3.micro",
    parameter_group_name="default.mysql5.7",
    storage_type="gp2",
    vpc_security_group_ids=[rds_sg.id],
    skip_final_snapshot=True,
    name=db_name,
    password=db_password,
    username=db_username
)

# Log groups
nginx_cw_group = aws.cloudwatch.LogGroup("ecs/zabbix/nginx")
zabbix_server_cw_group = aws.cloudwatch.LogGroup("ecs/zabbix/server")

# Prepare task definition and ECS service
nginx_container = Output.all("nginx", f"quitequiet/nginx-ubuntu-mysql:{nginx_tag}", 80, 80, nginx_cw_group.name, [
    {"name": "DB_SERVER_HOST", "value": mysql.address},
    {"name": "MYSQL_USER", "value": db_username},
    {"name": "MYSQL_PASSWORD", "value": db_password},
    {"name": "ZBX_SERVER_HOST", "value": zabbix_nlb.dns_name},
    {"name": "PHP_TZ", "value": "Europe/Kiev"}]) \
    .apply(lambda args: generate_container_definition(args[0], args[1], args[2], args[3], args[4], args[5]))

zabbix_container = Output.all("zabbix-server", f"quitequiet/zabbix-ubuntu-mysql:{zabbix_tag}", 10051, 10051, zabbix_server_cw_group.name, [
    {"name": "DB_SERVER_HOST", "value": mysql.address},
    {"name": "MYSQL_USER", "value": db_username},
    {"name": "MYSQL_PASSWORD", "value": db_password}]) \
    .apply(lambda args: generate_container_definition(args[0], args[1], args[2], args[3], args[4], args[5]))

# Spin up a load balanced service running our container image.
nginx_task_def = aws.ecs.TaskDefinition('nginx-task',
    family='fargate-task-definition',
    cpu='256',
    memory='512',
    network_mode='awsvpc',
    requires_compatibilities=['FARGATE'],
    execution_role_arn=role.arn,
    container_definitions=nginx_container
)

nginx_service = aws.ecs.Service('nginx-service',
    cluster=cluster.arn,
    desired_count=1,
    launch_type='FARGATE',
    task_definition=nginx_task_def.arn,
    network_configuration={
        'assign_public_ip': 'true',
        'subnets': default_vpc_subnets.ids,
        'security_groups': [nginx_sg.id]
    },
    load_balancers=[{
        'target_group_arn': atg.arn,
        'container_name': 'nginx',
        'container_port': 80
    }],
    opts=ResourceOptions(depends_on=[wl]),
)

zabbix_task_def = aws.ecs.TaskDefinition('zabbix-task',
    family='fargate-task-definition',
    cpu='256',
    memory='512',
    network_mode='awsvpc',
    requires_compatibilities=['FARGATE'],
    execution_role_arn=role.arn,
    container_definitions=zabbix_container
)

zabbix_service = aws.ecs.Service('zabbix-service',
    cluster=cluster.arn,
    desired_count=1,
    launch_type='FARGATE',
    task_definition=zabbix_task_def.arn,
    network_configuration={
        'assign_public_ip': 'true',
        'subnets': default_vpc_subnets.ids,
        'security_groups': [zabbix_sg.id]
    },
    load_balancers=[{
        'target_group_arn': zabbix_tg.arn,
        'container_name': 'zabbix-server',
        'container_port': 10051
    }],
    opts=ResourceOptions(depends_on=[nlb_listener]),
)


export('nginx', alb.dns_name)
export('zabbix', zabbix_nlb.dns_name)


def generate_container_definition(name, image, container_port, host_port, log_group_name, additional_envs):
    return json.dumps([{
        'name': name,
        'image': image,
        'portMappings': [{
            'containerPort': container_port,
            'hostPort': host_port,
            'protocol': 'tcp'
        }],
        'logConfiguration': {
            'logDriver': 'awslogs',
            'options': {
                    'awslogs-group': log_group_name,
                    'awslogs-region': aws.get_region().name,
                    'awslogs-stream-prefix': name
                }
            },
        'environment': additional_envs
    }])