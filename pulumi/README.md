# Pulumi code to deploy Zabbix in Fargate

Pulumi issues:

1. Shared folder on Windows inside Vagrant machine:

Not able to use default pulumi venv creation after command *pulumi new aws-python -s USERNAME/zabbix-ecs/dev* IF using shared folder in Vagrant machine. Reference https://github.com/pypa/pipenv/issues/2084 , https://stackoverflow.com/questions/24640819/protocol-error-setting-up-virtualenvironment-through-vagrant-on-ubuntu

Resolution:
- install virtualenv on machine: ```pip3 install virtualenv --user```
- create a folder, access it and create pulumi project: ```pulumi new aws-python -s <USERNAME>/zabbix-ecs/dev```
- create virtual env in the same folder using *--always-copy* to resolve venv issue: ```virtualenv -p python3 venv --always-copy```
- activate venv: ```source venv/bin/activate``` (NOTE: does not add "(venv)" to the bash prompt for some reason)
- install pulumi dependecies: ```pip3 install -r requirements.txt```
- when done working, use ```deactivate``` to stop working with virtual env

